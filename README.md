# Git 강의 참고 자료

* 강의 자료 - http://collab.lge.com/main/x/12HkTg
    * 종이 교재에 비해 개선하고 수정한 부분이 많으므로 위 링크 PDF를 반드시 참고

## 실습을 위한 사전 준비

* 실행환경
    * VPC Windows 10 가능
    * Windows 7, 10 가능 - 노트북, PC
    * macOS 가능
    * Linux - 터미널 기반 가능
    
* Git 설치
    * 공식 사이트 [https://git-scm.com/](https://git-scm.com/)
    * 강의 기준 버전 - 2.31 이상
    * 다운로드 페이지 https://git-scm.com/downloads
    * Windows 환경
        * Git 설치 프로그램 다운로드 후 설치
    * Linux 환경
        * 각 배포판의 패키지관리자를 통해 설치 https://git-scm.com/download/linux
    * Mac 환경
        * 기본으로 설치되어 있음
        * 업데이트 하려면 https://git-scm.com/download/mac 참고
    * Git 설치 후 터미널 설정 (선택)
        * [나눔고딕코딩 글꼴](https://fonts.google.com/download?family=Nanum%20Gothic%20Coding)
        * 옵션 - Looks - Theme - 'Flat UI'
        * 옵션 - Text - Font - NanumGothicCoding
    * 설치가 완료되어 사용 가능한 상태 화면
        * [git-install-ready.png](git-install-ready.png)

* Source Tree 설치
     * [Source Tree 사이트](https://www.sourcetreeapp.com/) 이동하여 설치 프로그램 다운로드 후 실행하여 설치
     * 강의 기준 버전 3.4
     * 만약 설치 중 .NET Framework 설치하라는 안내가 나오는 경우
         * [.NET Framework 사이트](https://dotnet.microsoft.com/download/dotnet-framework/net48) 방문하여 .NET Framework 4.8 Runtime 다운로드 하여 설치
         * 오프라인 설치 다운로드 링크 - https://dotnet.microsoft.com/download/dotnet-framework/thank-you/net48-offline-installer
         * .NET Framework 설치 후 OS 재시작 필요함
    * 설치가 완료되어 사용 가능한 상태 화면
        * [sourcetree-install-ready.png](sourcetree-install-ready.png)

## 실습 참고 자료

* 사전준비 https://bitbucket.org/git-prac/prac0-init
* 들어가며
    * [Git Cheatsheet 한글 PDF](https://bitbucket.org/git-prac/prac0-init/raw/e432293fbde156b238aa9d8600241c0e70e1b23e/git-cheatsheet-kr.pdf)
* 버전관리 개념과 모델 https://bitbucket.org/git-prac/prac1-reciep
    * SemVer https://semver.org/lang/ko/
    * 조선왕조실록 https://youtu.be/iHq7Zme1bPM?t=1373 , https://youtu.be/_Oki4FLQNgk?t=50
* 기본 버전관리 기능과 관련 Git 명령 https://bitbucket.org/git-prac/prac2-basic
    * https://git-scm.com/downloads/guis
* 브랜치와 머지 https://bitbucket.org/git-prac/prac3-branch
* 브랜치 관리 전략1 https://bitbucket.org/git-prac/prac4-branch
* 브랜치 관리 전략2 https://bitbucket.org/git-prac/prac5-branch
    * http://dogfeet.github.io/articles/2011/a-successful-git-branching-model.html
    * https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
    * https://guides.github.com/introduction/flow/
* 협업 워크플로우 https://bitbucket.org/git-prac/prac6-cowork
    * https://bitbucket.org/git-prac/prac6-cowork-2
    * https://bitbucket.org/git-prac/prac6-cowork-3
    * http://gerrit.webos.tv/git-prac/prac6-cowork
* 리뷰와 소통 시스템 https://bitbucket.org/git-prac/prac7-hub
    * http://gerrit.webos.tv
        * 저장소URL: http://gerrit.webos.tv/prac8-gerrit
        * 저장소URL: http://gerrit.webos.tv/play
    * https://github.com/git-prac-ex/git-prac-pr
        * https://github.com/seonghwanyee/git-prac-pr
* Git tips
    * 충돌 예제 https://github.com/git-prac-ex/prac-conflict
    * 바이너리 파일 비교 https://bitbucket.org/git-prac/prac-file/commits/7b3447fe6dc5ba29ef3df3e5777188fa4d4723db
* 더 읽어보기
    * 오픈소스와 민주주의와 GitHub 모델
        * [클레이 셔키(Clay Shirky): 어떻게 인터넷이 (어느 날엔가) 정부를 완전히 바꾸어 놓을 것인가.](https://www.ted.com/talks/clay_shirky_how_the_internet_will_one_day_transform_government?language=ko)
        * 대한민국 헌법 https://github.com/law-kr-jaryogujo/CONSTITUTION-OF-THE-REPUBLIC-OF-KOREA
        * 서울 정보소통광장 행정정보 공개 https://github.com/seoul-opengov/opengov

## Git 더 알아보기

* Pro Git - http://git-scm.org/book/ko/v2
* 지옥에서 온 Git - https://opentutorials.org/course/2708
* 책, 팀 개발을 위한 Git, GitHub 시작하기
* [Gerrit을 이용한 코드 리뷰 시스템 - 코드 리뷰와 Gerrit by Naver D2](https://d2.naver.com/helloworld/6033708)
* Git을 이용한 더 나은 버전관리 - https://speakerdeck.com/ibluemind/giteul-iyonghan-deo-naeun-beojeongwanri